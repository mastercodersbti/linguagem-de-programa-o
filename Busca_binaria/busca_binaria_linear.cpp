/*! \busca_binaria.cpp
 * Brief description: Main program (linear).
 *
 * Detailed description: Compare an int value with an entire array.
*/

#include <iostream>

using namespace std;

// Global scope
auto steps(0);

/* Function to print array */
void print_array( int A[ ], int sz )
{
	cout << "vet: [ ";
	for( auto i(0); i < sz ; ++i )
	{
		// Print array
		cout << A[i] << " ";
	}
	cout << "] " << endl << endl;
}

/* Function to search a specific element */
int binary_search( int* A, int elem, int low, int high )
{
	// If element isn't inside the array
	for ( auto i(low); i < high; ++i )
	{
		steps++;

		// Check where look for
		if ( A[i] == elem ) // Return the found element
			return i;
	}
	return -1;
}

int main()
{
	// Array input
	int v[] = {1, 2, 5, 10, 20, 25, 50, 65, 75, 89, 95, 99, 100, 102};
	int sz = sizeof(v)/sizeof(int); // Determine the array length
	int x = 95;

	// Print array
	print_array(v, sz);

	// Binary search
	cout << "<<< Search the element: " << x << endl;
	cout << "Result found in position: " << binary_search(v, x, 0, (sz - 1)) << " >>>" << endl;
	cout << "It takes " << steps << " steps to find the element" << endl << endl;

	// Program return
	return EXIT_SUCCESS;
}