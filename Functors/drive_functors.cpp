#include <iostream>
#include <vector>

// Criar um functor para definir critérios
struct CriteriaEven
{
	bool operator()( int _n ) const
	{
		return ( 0 == _n % 2 );
	}
};

// Critério: ser par
bool even( int _n )
{
	//return ( _n > 0 );
	return ( 0 == (_n % 2) );
}

// Critério: ser negativo
bool negative( int _n )
{
	return ( 0 == (_n < 0) );
}

#ifdef POINTER_FUNC
std::vector< int > findMatchingNumbers ( std::vector< int > _V, bool (*_pred)( int ) )
{
	// Armazena os números selecionados de acordo com o predicado
	std::vector< int > retVec;

	// Percorrer o vetor original e testar o predicado
	for ( auto & e : _V )
		if ( _pred( e ) )
			retVec.push_back( e );

	return retVec;
}
#else
template < typename FT >
std::vector< int > findMatchingNumbers ( std::vector< int > _V, FT _pred )
{
	// Armazena os números selecionados de acordo com o predicado
	std::vector< int > retVec;

	// Percorrer o vetor original e testar o predicado
	for ( auto & e : _V )
		if ( _pred( e ) )
			retVec.push_back( e );

	return retVec;
}
#endif

int main ( void )
{
	std::vector< int > V1;
	int myInts[] = { 23, -2, 1, -10, 3, 4, 11, -4, 2, 8, 7 };
	std::vector< int > V2( std::begin(myInts), std::end(myInts) );

	//std::vector< int > V3 = { {23}, {2}, {1}, {3}, {5} };

	std::cout << ">>> Original vector: [ ";
	for ( auto & e : V2 )
		std::cout << e << " ";
	std::cout << "]\n";

	CriteriaEven even;
	V1 = findMatchingNumbers( V2, even );
	//V1 = findMatchingNumbers( V2, even );

	std::cout << ">>> Matching criteria: [ ";
	for ( auto & e : V1 )
		std::cout << e << " ";
	std::cout << "]\n";

	return EXIT_SUCCESS;
}