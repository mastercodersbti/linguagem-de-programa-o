/*! \busca_binaria_ponteiro.cpp
 * Brief description: Main program (recursive pointer).
 *
 * Detailed description: Compare an int value with an entire array
 * using a pointer as a parameter.
*/

#include <iostream>

using namespace std;

// Global scope
auto steps(0);

// Struct definition
struct Person
{
	string name;
	int age;
};

/* Function to print array  person*/
void print_array_person( Person *A, int sz )
{
	cout << "vet: [ ";
	for( auto i(0); i < sz ; ++i )
	{
		// Print array person
		cout << A[i].name << " : " << A[i].age << " ";
	}
	cout << "] " << endl << endl;
}

/* Function to print array */
void print_array( int A[ ], int sz )
{
	cout << "vet: [ ";
	for( auto i(0); i < sz ; ++i )
	{
		// Print array
		cout << A[i] << " ";
	}
	cout << "] " << endl << endl;
}

/* Define a generic pointer type */
typedef int (*compare) (void *, void *);

/* Function to search a specific element */
int binary_search( void A[], void *elem, int low, int high, compare comp)
{
	steps += 1;

	// If element isn't inside the array
	if ( low > high )
		return -1;

	// Find the middle element
	auto mid = (low + high) / 2;

	// Get result from compare function
	int result = comp(&A[mid], elem);

	// Check where look for
	if ( result == 0 ) // Return the middle element (success)
		return mid;
	else if ( resut > 0 ) // The element is on the right of mid
	{
		return binary_search(A, elem, (mid+1), high);
	}
	else // The element is on the left of mid
	{
		return binary_search(A, elem, low, (mid - 1));	
	}
}

int main()
{
	// Array input
	int v[] = {1, 2, 5, 10, 20, 25, 50, 65, 75, 89, 95, 99, 100, 102};
	int sz = sizeof(v)/sizeof(int); // Determine the array length
	int x = 99;

	// Generic array
	Person v2[] = {{"Igor", 23}, 
				   {"Robert", 54},
				   {"Leandro", 30},
				   {"João", 18},
				   {"Maria", 20}};
	int sz2 = sizeof(v2)/sizeof(int); // Determine the array length
	int y = 0;

	// Print array
	print_array(v, sz);

	// Binary search
	cout << "<<< Search the element: " << x << endl;
	cout << "Result found in position: " << binary_search(v, x, 0, (sz - 1)) << " >>>" << endl;
	cout << "It takes " << steps << " steps to find the element" << endl << endl;

	// Print array
	print_array_person(v2, sz2);

	// Binary search
	//cout << "<<< Search the element: " << x << endl;
	//cout << "Result found in position: " << binary_search(v2, y, 0, (sz - 1)) << " >>>" << endl;
	//cout << "It takes " << steps << " steps to find the element" << endl << endl;

	// Program return
	return EXIT_SUCCESS;
}