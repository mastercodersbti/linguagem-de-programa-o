/*! \busca_binaria.cpp
 * Brief description: Main program (pointer).
 *
 * Detailed description: Compare an int value with an entire array.
*/

#include <iostream>

using namespace std;

// Global scope
auto steps(0);

/* Compare functions */
int max ( int a, int b ) { return ( a > b ? a : b ); }
int min ( int a, int b ) { return ( a < b ? a : b ); }
int pri ( int a, int b ) { return a; }
int ult ( int a, int b ) { return b; }
int som ( int a, int b ) { return (a + b); }

/* Define a generic pointer type */
typedef int (*functionPointer) (int, int);

void print_func( int a, int b, functionPointer func, string func_name )
{
	cout << endl << "----------------------------------------" << endl;
	cout << func_name << endl;
	cout << ">>> a: " << a << " b: " << b << endl;
	cout << ">>> Func (a, b): " << func(a, b) << endl;
	cout << "----------------------------------------" << endl;
}

int main ()
{
	// Auxiliary variables
	int x = 5, y = 10;

	// Print results
	print_func(x, y, min, "Minimo");
	print_func(x, y, max, "Maximo");
	print_func(x, y, pri, "Primeiro");
	print_func(x, y, ult, "Ultimo");
	print_func(x, y, som, "Soma");
	cout << endl;

	// Program return
	return EXIT_SUCCESS;
}