#include <iostream>
#include <queue>
#include <stack>

using namespace std;

/* PROTOTIPO DAS FUNCOES */
bool 		isOperating( char _c );
bool 		hasPriority(char _a, char _b);
queue<char>	infixToPostfix( queue<char> _inputQueue );

//#define TESTE_1
//#define TESTE_2
//define TESTE_3
#define TESTE_4
//#define TESTE_5
//#define TESTE_6

int main()
{
	// Limpa a tela 
	//system('cls');
	queue<char> teste;
	queue<char> entryQueue;

	#ifdef TESTE_1
	// A + B
	entryQueue.push('1');
	entryQueue.push('+');
	entryQueue.push('2');
	cout << "entrada 1 + 2" << endl;
	cout << "SAIDA ESPERADA 12+" << endl;
	#endif 

	#ifdef TESTE_2
	// A + B - C
	entryQueue.push('1');
	entryQueue.push('+');
	entryQueue.push('2');
	entryQueue.push('-');
	entryQueue.push('3');
	cout << "Entrada 1+2-3" << endl;
	cout << "Saida esperada 12+3-" << endl;
	#endif

	#ifdef TESTE_3
	// (A + B) ∗ (C − D) 	==> Entrada
	// AB + CD − ∗ 			==> Saida Esperada
	entryQueue.push('(');
	entryQueue.push('1');
	entryQueue.push('+');
	entryQueue.push('2');
	entryQueue.push(')');
	entryQueue.push('*');
	entryQueue.push('(');
	entryQueue.push('3');
	entryQueue.push('-');
	entryQueue.push('4');
	entryQueue.push(')');
	cout << "ENTRADA (1+2)*(3-4)" << endl;
	cout << "SAIDA ESPERADA 12+34-*" << endl;
	#endif
	
	#ifdef TESTE_4
	// A + B * C 	==> Entrada
	// ABC*+ 		==> Saida esperada
	entryQueue.push('1');
	entryQueue.push('+');
	entryQueue.push('2');
	entryQueue.push('*');
	entryQueue.push('3');
	cout << "Entrada 1 + 2 * 3" << endl;
	cout << "Saida esperada = 123*+" << endl;
	#endif

	#ifdef TESTE_5
	// (A * B) + C 	==> Entrada
	// AB*C+ 		==> Saida esperada
	entryQueue.push('(');
	entryQueue.push('1');
	entryQueue.push('*');
	entryQueue.push('2');
	entryQueue.push(')');
	entryQueue.push('+');
	entryQueue.push('3');
	cout << "Entrada (1 * 2) + 3" << endl;
	cout << "Saida esperada = 12*3+" << endl;
	#endif

	#ifdef TESTE_6
	// A * (B + C) 	==> Entrada
	// ABC+* 		==> Saida esperada
	entryQueue.push('(');
	entryQueue.push('1');
	entryQueue.push('*');
	entryQueue.push('2');
	entryQueue.push(')');
	entryQueue.push('+');
	entryQueue.push('3');
	cout << "Entrada 1 * (2 + 3)" << endl;
	cout << "Saida esperada = 123+*" << endl;
	#endif

	// A + B * C
	/*
	entryQueue.push('1');
	entryQueue.push('+');
	entryQueue.push('2');
	entryQueue.push('*');
	entryQueue.push('3');
	*/
	// A * B + C
	
	
	/*
	entryQueue.push('(');
	entryQueue.push('1');
	entryQueue.push('+');
	entryQueue.push('2');
	entryQueue.push(')');
	entryQueue.push('*');
	entryQueue.push('(');
	entryQueue.push('3');
	entryQueue.push('+');
	entryQueue.push('4');
	entryQueue.push(')');
	*/

	teste = infixToPostfix(entryQueue);

	while(!teste.empty())
	{
		cout << teste.front();
		teste.pop();
	}
	cout << endl;
	return EXIT_SUCCESS;
}


bool isOperating( char _c )
{
	/*! boleano que sera retornado */
	bool bResult = false;
	/*! String contendo a lista de operandos */
	string operands = "0123456789";
    
    /*! Itera sobre a string procurando se _c eh ou nao um operando */
    for(auto it(operands.begin()) ; it <=operands.end(); ++it )
    {
        if(_c == *it )
            bResult = true;
    }
    /*! Retorna true se _c eh operando ou false caso contrario*/
    return bResult;
}


bool hasPriority(char _a, char _b)
{
	bool bResult;

    int auxA; /*!< Auxilia a classificar o _a */
    int auxB;   /*!< Auxilia a classificar o _b */

    /*! Classifica a precedencia de _a */
    if( _a == '(' || _a == ')' )
        auxA = 0;
    if( _a == '^' )
        auxA = 3;
    if( _a == '*' || _a == '/' || _a == '%' )
        auxA = 2;
    if( _a == '+' || _a == '-')
        auxA = 1;

    /*! Classifica a precedencia de _b */
    if( _b == '(' || _b == ')')
        auxB = 0;
    if( _b == '^' )
        auxB = 3;
    if( _b == '*' || _b == '/' || _b == '%' )
        auxB = 2;
    if( _b == '+' || _b == '-')
        auxB = 1;

    /*! Avalia a precedencia de acordo com a classificacao recebida */
    if( auxA >= auxB )
        bResult = true;
    else
        bResult = false;

    return bResult;
}


queue<char> infixToPostfix( queue<char> _inputQueue )
{
	char symbol;					/*!< O simbolo atual a ser classificado */
	char topSymbol;					/*!< O simbolo do top da pila de operadores */
	queue<char> outputQueue; 		/*!< Lista de saida com o formato posfixo */
	stack<char> stackOfOperators;	/*!< Pilha de operadores */
	
	/*! Enquanto não chegar ao fim da fila de entrada faca */
	while( !_inputQueue.empty() )
	{
		/** Remover o simbolo da lista de entrada e armazenar em symbol */
		symbol = _inputQueue.front();
		_inputQueue.pop();

		/*! Se symbol for operando... entao */
		if ( isOperating( symbol ) )
		{
			/* Enviar symbol direto para a fila de saida */
			outputQueue.push( symbol );
		}
		else if( symbol == '(' ) /*!< Caso o symbol seja um abre parenteses */
		{
			/*! Joga o symbol na pilha */
			stackOfOperators.push( symbol );
		}
		else 	/* Se symbol nao for operando nem abre parenteses */
		{	
			/*! 
			 *	Se a pilha de operadores nao esta vazia 
			 *	topSymbol recebe pela primeira vez o elemento do topo para poder
			 * 	entrar no proximo while
			 */
			if(!stackOfOperators.empty() )
			{
				/*! topSymbol recebe o simbolo do topo da pilha de operadores */
				topSymbol = stackOfOperators.top();
			}

			/*!
			 * 	Enquanto a pilha de operadores nao estiver vazia 
			 *	e o simbolo do topo (topSymbol) ≥ symb faca... 
			 */
			while( !stackOfOperators.empty() && hasPriority( topSymbol, symbol ) )
			{
				/*! topSymbol recebe o simbolo do topo da pilha de operadores */
				topSymbol = stackOfOperators.top();

				/* Se o simbolo do topo da pilha nao for parentesis */
				if( topSymbol != '(' && topSymbol != ')')
				{
					outputQueue.push( topSymbol );	/*!< Insere o simbolo do topo da pilha na fila de saida */
					stackOfOperators.pop(); 		/*!< Remove o operador ja utilizado da pilha */
				}
				else /*!< Se for um parentesis, apenas o retira da pilha */
				{
					stackOfOperators.pop();
				}	
			}

			/*! Empilhar symbol depois que retirar operadores de precedencia ≥ */
			stackOfOperators.push( symbol );
		}
	}

	/*! Descarregar operadores remanescentes da pilha e manda-los para a fila de saida */
	while( !stackOfOperators.empty() )
	{
		/*! Remover simbolo da pilha e enviar para fila de saida */
		topSymbol = stackOfOperators.top();
		stackOfOperators.pop();
		/*! Os simbolos parenteses nao entram para a fila de saida */
		if( topSymbol != '(' && topSymbol != ')' )
		{
			outputQueue.push( topSymbol ); /*! Se nao for parentesis, vai para a fila de saida */
		}
	}

	/*! Retorna a fila em formato posfixo */
	return outputQueue;
}