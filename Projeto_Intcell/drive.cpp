#include <iostream>
#include <cassert>
#include "IntCell.h"

using namespace std;

int main()
{
	IntCell a;
	IntCell b(100);
	IntCell c, d;

	cout << ">>> Valor de a: " << a.get() << endl;
	cout << ">>> Valor de b: " << b.get() << endl;
	cout << ">>> Valor de b: " << c.get() << endl;
	cout << ">>> Valor de b: " << d.get() << endl;
	cout << "___________________________" << endl << endl;

	a.write(5);
	b.write(200);
	//c.write((a.get() + b.get()));

	c = a + b;
	d.write(a.get() + b.get());
	assert(c.get() == d.get());
	assert(c == d);

	cout << ">>> Valor de a: " << a.get() << endl;
	cout << ">>> Valor de b: " << b.get() << endl;
	cout << ">>> Valor de c: " << c.get() << endl;
	cout << ">>> Valor de d: " << d.get() << endl;

	return 0;
}