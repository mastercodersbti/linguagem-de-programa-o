#ifndef _INTCELL_H_
#define _INTCELL_H_

class IntCell
{
	public:
						IntCell( int _newVal = -1 );
		void 			write( int _newVal );
		int 			get() const;
		IntCell 		operator+( const IntCell & _rhs );
		IntCell 		operator-( const IntCell & _rhs );
		IntCell 		operator*( const IntCell & _rhs );
		IntCell 		operator/( const IntCell & _rhs );
		bool		 	operator==( const IntCell & _rhs );
		bool 			operator!=( const IntCell & _rhs );
		bool 			operator>( const IntCell & _rhs );
		bool 			operator<( const IntCell & _rhs );

	private:
		int m_val;
};

#endif