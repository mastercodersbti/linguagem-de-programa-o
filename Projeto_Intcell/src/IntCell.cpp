#include "IntCell.h"

IntCell::IntCell( int _initVal )
{
	m_val = _initVal;
}

void IntCell::write( int _newVal )
{
	m_val = _newVal;
}

int IntCell::get( void ) const
{
	return m_val;
}

IntCell IntCell::operator+( const IntCell & _rhs )
{
	// Calcular a soma em uma variável temporária
	IntCell resultado;
	resultado.m_val = m_val + _rhs.m_val;

	// Retorna a soma
	return resultado;
}

IntCell IntCell::operator-( const IntCell & _rhs )
{
	// Calcular a soma em uma variável temporária
	IntCell resultado;
	resultado.m_val = m_val - _rhs.m_val;

	// Retorna a soma
	return resultado;
}

IntCell IntCell::operator*( const IntCell & _rhs )
{
	// Calcular a soma em uma variável temporária
	IntCell resultado;
	resultado.m_val = m_val - _rhs.m_val;

	// Retorna a soma
	return resultado;
}

IntCell IntCell::operator/( const IntCell & _rhs )
{
	// Calcular a soma em uma variável temporária
	IntCell resultado;
	resultado.m_val = m_val - _rhs.m_val;

	// Retorna a soma
	return resultado;
}

bool IntCell::operator==( const IntCell & _rhs )
{
	return ( this->m_val == _rhs.m_val );
}

bool IntCell::operator!=( const IntCell & _rhs )
{
	return ( this->m_val != _rhs.m_val );
}

bool IntCell::operator>( const IntCell & _rhs )
{
	return ( this->m_val > _rhs.m_val );
}

bool IntCell::operator<( const IntCell & _rhs )
{
	return ( this->m_val < _rhs.m_val );
}